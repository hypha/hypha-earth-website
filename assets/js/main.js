/*! main.js | SEEDS | Digital Mycelium */

/* ==========================================================================
Website core JS file
========================================================================== */


$(document).ready(function ($) {

    "use strict";





function showDiv() {
    // If there are hidden divs left
    if($('.wavy-icon-box').length) {
        // Fade the first of them in
        $('.wavy-icon-box:hidden:first').fadeIn();
        // And wait one second before fading in the next one
        setTimeout(showDiv, 1000);
    }
}




/* ANIMATED HOMEPAGE TEXT */
const resolver = {
  resolve: function resolve(options, callback) {
    // The string to resolve
    const resolveString = options.resolveString || options.element.getAttribute('data-target-resolver');
    const combinedOptions = Object.assign({}, options, { resolveString: resolveString });

    function getRandomInteger(min, max) {
      return Math.floor(Math.random() * (max - min + 1)) + min;
    };

    function randomCharacter(characters) {
      return characters[getRandomInteger(0, characters.length - 1)];
    };

    function doRandomiserEffect(options, callback) {
      const characters = options.characters;
      const timeout = options.timeout;
      const element = options.element;
      const partialString = options.partialString;

      let iterations = options.iterations;

      setTimeout(() => {
        if (iterations >= 0) {
          const nextOptions = Object.assign({}, options, { iterations: iterations - 1 });

          // Ensures partialString without the random character as the final state.
          if (iterations === 0) {
            element.textContent = partialString;
          } else {
            // Replaces the last character of partialString with a random character
            element.textContent = partialString.substring(0, partialString.length - 1) + randomCharacter(characters);
          }

          doRandomiserEffect(nextOptions, callback);
        } else if (typeof callback === "function") {
          callback();
        }
      }, options.timeout);
    };

    function doResolverEffect(options, callback) {
      const resolveString = options.resolveString;
      const characters = options.characters;
      const offset = options.offset;
      const partialString = resolveString.substring(0, offset);
      const combinedOptions = Object.assign({}, options, { partialString: partialString });

      doRandomiserEffect(combinedOptions, () => {
        const nextOptions = Object.assign({}, options, { offset: offset + 1 });

        if (offset <= resolveString.length) {
          doResolverEffect(nextOptions, callback);
        } else if (typeof callback === "function") {
          callback();
        }
      });
    };

    doResolverEffect(combinedOptions, callback);
  } };


/* Some GLaDOS quotes from Portal 2 chapter 9: The Part Where He Kills You
        * Source: http://theportalwiki.com/wiki/GLaDOS_voice_lines#Chapter_9:_The_Part_Where_He_Kills_You
        */
const strings = [
'We build dapps and tools to facilitate humanity\'s transition to a globally regenerative and thriving civilization',
'...'];


let counter = 0;

const options = {
  // Initial position
  offset: 0,
  // Timeout between each random character
  timeout: 5,
  // Number of random characters to show
  iterations: 10,
  // Random characters to pick from
  characters: ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'x', 'y', 'x', '#', '%', '&', '-', '+', '_', '?', '/', '\\', '='],
  // String to resolve
  resolveString: strings[counter],
  // The element
  element: document.querySelector('[data-target-resolver]') };


// Callback function when resolve completes
function callback() {
  setTimeout(() => {
    counter++;

    if (counter >= strings.length) {
      counter = 0;
    }

    let nextOptions = Object.assign({}, options, { resolveString: strings[counter] });
    //resolver.resolve(nextOptions, callback);
    showDiv();
    $([document.documentElement, document.body]).animate({
        scrollTop: $(".projects-container").offset().top
    }, 4000);

  }, 1000);
}

resolver.resolve(options, callback);

    //Video embed init
    if ($('#video-embed').length) {
        Video('#video-embed');
    }

    //Cover video
    if ($('.covervid-video').length) {
        $('.covervid-video').coverVid(1920, 1080);
    }


    //Page loader
    if ($('.pageloader').length)
    {

        $('.pageloader').toggleClass('is-active');

        $(window).on('load', function () {
            var pageloaderTimeout = setTimeout(function () {
                $('.pageloader').fadeOut();
                $('.infraloader').fadeOut();

                $('body').removeClass('is-fixed').finish();
                clearTimeout(pageloaderTimeout);

            }, 1700);
        })
    }


    //Mobile menu toggle
    if ($('.custom-burger').length) {
        $('.custom-burger').on("click", function () {
            $(this).toggleClass('is-active');
            if ($('.navbar-menu').hasClass('is-active')) {
                $('.navbar-menu').removeClass('is-active');
                $('.navbar-fade.navbar-light').removeClass('is-dark-mobile');
            } else {
                $('.navbar-menu').addClass('is-active');
                $('.navbar-fade.navbar-light').addClass('is-dark-mobile');
            }
            //Revert navbar to initial color state
            if ($('.navbar-faded').hasClass('is-dark-mobile')) {
                $('.navbar-faded').removeClass('is-dark-mobile');
            }
            $('.navbar.is-static').toggleClass('is-dark-mobile');
        });
    }

    


    //Navbar fade
    if ($('.navbar-wrapper.navbar-fade.navbar-light').length)
    {
        $(".navbar-wrapper.navbar-fade").wrap('<div class="navbar-placeholder"></div>');
        $(".navbar-placeholder").height(jQuery(".navbar-wrapper.navbar-fade").outerHeight());
        $(window).on('scroll', function () {    // this will work when your window scrolled.
            var height = $(window).scrollTop();  //getting the scrolling height of window
            if (height > 65) {
                $(".navbar-wrapper.navbar-fade.is-transparent").removeClass('is-transparent navbar-light').addClass('navbar-faded');
            } else {
                $(".navbar-wrapper").removeClass('navbar-faded').addClass('is-transparent navbar-light');
            }
        });
    }


    // Back to Top button behaviour
    var pxShow = 600;
    var scrollSpeed = 500;
    $(window).on('scroll', function () {
        if ($(window).scrollTop() >= pxShow) {
            $("#backtotop").addClass('visible');
        } else {
            $("#backtotop").removeClass('visible');
        }
    });
    $('#backtotop a').on('click', function () {
        $('html, body').animate({
            scrollTop: 0
        }, scrollSpeed);
        return false;
    });

    // Scroll to hash
    $('a[href*="#"]')
    // Remove links that don't actually link to anything
    .not('[href="#"]')
    .not('[href="#0"]')
    .on('click', function (event)
    {
        // On-page links
        if (
            location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '')
            &&
            location.hostname == this.hostname
        )
        {
            // Figure out element to scroll to
            var target = $(this.hash);
            target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
            // Does a scroll target exist?
            if (target.length)
            {
                // Only prevent default if animation is actually gonna happen
                event.preventDefault();
                $('html, body').animate({
                    scrollTop: target.offset().top
                }, 550, function () {
                    // Callback after animation
                    // Must change focus!
                    var $target = $(target);
                    $target.focus();
                    if ($target.is(":focus")) { // Checking if the target was focused
                        return false;
                    } else {
                        $target.attr('tabindex', '-1'); // Adding tabindex for elements not focusable
                        $target.focus(); // Set focus again
                    };
                });
            }
        }
    });



/* ==========================================================================
START MAILCHIMP
========================================================================== */
/*
    $(".mailchimp-form").on("click", function(){
            
        if ( $('#mce-success-response:visible').length !== 0  )
        {
            console.log('!== success')
            //$('#mc_embed_signup_scroll .control-material, #mc_embed_signup .title').hide();
            $('#mce-success-response').css('margin', "0");
        }
        else if ( $('#mce-error-response:visible').length !== 0 )
        {
            console.log('!== error')
            //$('#seeds-signup-form-modal').scrollTop(0);
            $('#mce-error-response').css('margin', "0");
        }
    });
*/
/* ==========================================================================
END MAILCHIMP
========================================================================== */










/* ==========================================================================
START Bulma modals implementation
========================================================================== */

    //main variable
    var modalID;
    
    //Triggering a modal
    $('.modal-trigger').on("click", function(link){
        link.preventDefault();
        modalID = $(this).attr('data-modal');
        $('#' + modalID).toggleClass('is-active');
        $('#' + modalID + ' .modal-background').toggleClass('scaleInCircle');
        $('#' + modalID + ' .modal-content').toggleClass('scaleIn');
        $('#' + modalID + ' .modal-close').toggleClass('is-hidden');
        //Prevent sticky fixed nav and backtotop from overlapping modal
        $('#scrollnav, #backtotop').toggleClass('is-hidden');
        //Prevent body from scrolling when scrolling inside modal
        if( $('#' + modalID).hasClass( "is-active" ) )
        {
            $('html').addClass('is-clipped').stop( true, true );
        }
    });

    //Closing a modal
    //vmodha: added 'modal-background' as a modal close trigger
    $('.modal-close, .modal-dismiss, .modal-background').on("click", function(){
        $('#' + modalID + ' .modal-background').toggleClass('scaleInCircle');
        $('#' + modalID + ' .modal-content').toggleClass('scaleIn');
        $('#' + modalID + ' .modal-close').toggleClass('is-hidden');
        //Restore native body scroll
        $('.modal.is-active').removeClass('is-active');

        $('#scrollnav, #backtotop').toggleClass('is-hidden');
        
        if( !$('#' + modalID).hasClass( "is-active" ) )
        {
            $('html').removeClass('is-clipped').stop( true, true );
        }
        
    })
/* ==========================================================================
END Bulma modals implementation
========================================================================== */




    //Toggle the sign up button color when solid navbar comes in
    if ($('.navbar-light').length) {
        $(window).on('scroll', function () {    // this will work when your window scrolled.
            var height = $(window).scrollTop();  //getting the scrolling height of window
            if(height  > 80) {
                $('.button-signup').removeClass('light-btn').addClass('primary-btn');
            } else{
                $('.button-signup').removeClass('primary-btn').addClass('light-btn');
            }
        }); 
    }
    
    //Scroll reveal definitions
    // Declaring defaults
    window.sr = ScrollReveal();

    // Simple reveal
    sr.reveal('.is-title-reveal', { 
        origin: 'bottom',
        distance: '20px',
        duration: 600,
        delay: 100,
        rotate: { x: 0, y: 0, z: 0 },
        opacity: 0,
        scale: 1,
        easing: 'cubic-bezier(0.215, 0.61, 0.355, 1)',
        container: window.document.documentElement,
        mobile: true,
        reset: false,
        useDelay: 'always',
        viewFactor: 0.2,

    });
    
    // Left reveal
    sr.reveal('.is-left-reveal', { 
        origin: 'left',
        distance: '20px',
        duration: 500,
        delay: 150,
        rotate: { x: 0, y: 0, z: 0 },
        opacity: 0,
        scale: 1,
        easing: 'cubic-bezier(0.215, 0.61, 0.355, 1)',
        container: window.document.documentElement,
        mobile: true,
        reset: false,
        useDelay: 'always',
        viewFactor: 0.4,

    });
    
    // Right reveal
    sr.reveal('.is-right-reveal', { 
        origin: 'right',
        distance: '20px',
        duration: 500,
        delay: 150,
        rotate: { x: 0, y: 0, z: 0 },
        opacity: 0,
        scale: 1,
        easing: 'cubic-bezier(0.215, 0.61, 0.355, 1)',
        container: window.document.documentElement,
        mobile: true,
        reset: false,
        useDelay: 'always',
        viewFactor: 0.4,

    });

    // Revealing multiple icons
    sr.reveal('.is-icon-reveal', { 
        origin: 'bottom',
        distance: '20px',
        duration: 600,
        delay: 100,
        rotate: { x: 0, y: 0, z: 0 },
        opacity: 0,
        scale: 1,
        easing: 'cubic-bezier(0.215, 0.61, 0.355, 1)',
        container: window.document.documentElement,
        mobile: true,
        reset: true,
        useDelay: 'always',
        viewFactor: 0.2,

    }, 300);

    // Revealing multiple posts
    sr.reveal('.is-post-reveal', { 
        origin: 'bottom',
        distance: '20px',
        duration: 600,
        delay: 100,
        rotate: { x: 0, y: 0, z: 0 },
        opacity: 0,
        scale: 1,
        easing: 'cubic-bezier(0.215, 0.61, 0.355, 1)',
        container: window.document.documentElement,
        mobile: true,
        reset: false,
        useDelay: 'always',
        viewFactor: 0.2,

    }, 160);

    // Revealing multiple cards
    sr.reveal('.is-card-reveal', { 
        origin: 'bottom',
        distance: '20px',
        duration: 600,
        delay: 100,
        rotate: { x: 0, y: 0, z: 0 },
        opacity: 0,
        scale: 1,
        easing: 'cubic-bezier(0.215, 0.61, 0.355, 1)',
        container: window.document.documentElement,
        mobile: true,
        reset: false,
        useDelay: 'always',
        viewFactor: 0.2,

    }, 160);
    
    // Revealing multiple dots
    sr.reveal('.is-dot-reveal', { 
        origin: 'bottom',
        distance: '20px',
        duration: 600,
        delay: 100,
        rotate: { x: 0, y: 0, z: 0 },
        opacity: 0,
        scale: 1,
        easing: 'cubic-bezier(0.215, 0.61, 0.355, 1)',
        container: window.document.documentElement,
        mobile: true,
        reset: true,
        useDelay: 'always',
        viewFactor: 0.2,

    }, 160);


})






/*!
 * current-device v0.8.2 - https://github.com/matthewhudson/current-device
 * MIT Licensed
 */
!function (n, e) { "object" == typeof exports && "object" == typeof module ? module.exports = e() : "function" == typeof define && define.amd ? define([], e) : "object" == typeof exports ? exports.device = e() : n.device = e() }(window, function () { return function (n) { var e = {}; function o(t) { if (e[t]) return e[t].exports; var r = e[t] = { i: t, l: !1, exports: {} }; return n[t].call(r.exports, r, r.exports, o), r.l = !0, r.exports } return o.m = n, o.c = e, o.d = function (n, e, t) { o.o(n, e) || Object.defineProperty(n, e, { enumerable: !0, get: t }) }, o.r = function (n) { "undefined" != typeof Symbol && Symbol.toStringTag && Object.defineProperty(n, Symbol.toStringTag, { value: "Module" }), Object.defineProperty(n, "__esModule", { value: !0 }) }, o.t = function (n, e) { if (1 & e && (n = o(n)), 8 & e) return n; if (4 & e && "object" == typeof n && n && n.__esModule) return n; var t = Object.create(null); if (o.r(t), Object.defineProperty(t, "default", { enumerable: !0, value: n }), 2 & e && "string" != typeof n) for (var r in n) o.d(t, r, function (e) { return n[e] }.bind(null, r)); return t }, o.n = function (n) { var e = n && n.__esModule ? function () { return n.default } : function () { return n }; return o.d(e, "a", e), e }, o.o = function (n, e) { return Object.prototype.hasOwnProperty.call(n, e) }, o.p = "", o(o.s = 0) }([function (n, e, o) { n.exports = o(1) }, function (n, e, o) { "use strict"; o.r(e); var t = "function" == typeof Symbol && "symbol" == typeof Symbol.iterator ? function (n) { return typeof n } : function (n) { return n && "function" == typeof Symbol && n.constructor === Symbol && n !== Symbol.prototype ? "symbol" : typeof n }, r = window.device, i = {}, a = []; window.device = i; var c = window.document.documentElement, d = window.navigator.userAgent.toLowerCase(), u = ["googletv", "viera", "smarttv", "internet.tv", "netcast", "nettv", "appletv", "boxee", "kylo", "roku", "dlnadoc", "pov_tv", "hbbtv", "ce-html"]; function l(n, e) { return -1 !== n.indexOf(e) } function s(n) { return l(d, n) } function f(n) { return c.className.match(new RegExp(n, "i")) } function b(n) { var e = null; f(n) || (e = c.className.replace(/^\s+|\s+$/g, ""), c.className = e + " " + n) } function p(n) { f(n) && (c.className = c.className.replace(" " + n, "")) } function w() { i.landscape() ? (p("portrait"), b("landscape"), m("landscape")) : (p("landscape"), b("portrait"), m("portrait")), h() } function m(n) { for (var e in a) a[e](n) } i.macos = function () { return s("mac") }, i.ios = function () { return i.iphone() || i.ipod() || i.ipad() }, i.iphone = function () { return !i.windows() && s("iphone") }, i.ipod = function () { return s("ipod") }, i.ipad = function () { return s("ipad") }, i.android = function () { return !i.windows() && s("android") }, i.androidPhone = function () { return i.android() && s("mobile") }, i.androidTablet = function () { return i.android() && !s("mobile") }, i.blackberry = function () { return s("blackberry") || s("bb10") || s("rim") }, i.blackberryPhone = function () { return i.blackberry() && !s("tablet") }, i.blackberryTablet = function () { return i.blackberry() && s("tablet") }, i.windows = function () { return s("windows") }, i.windowsPhone = function () { return i.windows() && s("phone") }, i.windowsTablet = function () { return i.windows() && s("touch") && !i.windowsPhone() }, i.fxos = function () { return (s("(mobile") || s("(tablet")) && s(" rv:") }, i.fxosPhone = function () { return i.fxos() && s("mobile") }, i.fxosTablet = function () { return i.fxos() && s("tablet") }, i.meego = function () { return s("meego") }, i.cordova = function () { return window.cordova && "file:" === location.protocol }, i.nodeWebkit = function () { return "object" === t(window.process) }, i.mobile = function () { return i.androidPhone() || i.iphone() || i.ipod() || i.windowsPhone() || i.blackberryPhone() || i.fxosPhone() || i.meego() }, i.tablet = function () { return i.ipad() || i.androidTablet() || i.blackberryTablet() || i.windowsTablet() || i.fxosTablet() }, i.desktop = function () { return !i.tablet() && !i.mobile() }, i.television = function () { for (var n = 0; n < u.length;) { if (s(u[n])) return !0; n++ } return !1 }, i.portrait = function () { return screen.orientation && Object.prototype.hasOwnProperty.call(window, "onorientationchange") ? l(screen.orientation.type, "portrait") : window.innerHeight / window.innerWidth > 1 }, i.landscape = function () { return screen.orientation && Object.prototype.hasOwnProperty.call(window, "onorientationchange") ? l(screen.orientation.type, "landscape") : window.innerHeight / window.innerWidth < 1 }, i.noConflict = function () { return window.device = r, this }, i.ios() ? i.ipad() ? b("ios ipad tablet") : i.iphone() ? b("ios iphone mobile") : i.ipod() && b("ios ipod mobile") : i.macos() ? b("macos desktop") : i.android() ? i.androidTablet() ? b("android tablet") : b("android mobile") : i.blackberry() ? i.blackberryTablet() ? b("blackberry tablet") : b("blackberry mobile") : i.windows() ? i.windowsTablet() ? b("windows tablet") : i.windowsPhone() ? b("windows mobile") : b("windows desktop") : i.fxos() ? i.fxosTablet() ? b("fxos tablet") : b("fxos mobile") : i.meego() ? b("meego mobile") : i.nodeWebkit() ? b("node-webkit") : i.television() ? b("television") : i.desktop() && b("desktop"), i.cordova() && b("cordova"), i.onChangeOrientation = function (n) { "function" == typeof n && a.push(n) }; var y = "resize"; function v(n) { for (var e = 0; e < n.length; e++)if (i[n[e]]()) return n[e]; return "unknown" } function h() { i.orientation = v(["portrait", "landscape"]) } Object.prototype.hasOwnProperty.call(window, "onorientationchange") && (y = "orientationchange"), window.addEventListener ? window.addEventListener(y, w, !1) : window.attachEvent ? window.attachEvent(y, w) : window[y] = w, w(), i.type = v(["mobile", "tablet", "desktop"]), i.os = v(["ios", "iphone", "ipad", "ipod", "android", "blackberry", "macos", "windows", "fxos", "meego", "television"]), h(), e.default = i }]).default });





